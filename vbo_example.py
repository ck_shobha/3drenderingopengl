from OpenGL import GL, GLUT, GLU
import numpy as np
from OpenGL.arrays import vbo


class DemoVBO(object):
    def __init__(self, nvert=100, jiggle=0.01):
        self.nvert = nvert
        self.jiggle = jiggle
        vert = 2*np.random.rand(nvert, 2, int) -1
        self.verts = np.require(vert, np.float64, 'F')
        self.vertexbo = vbo.VBO(self.verts)

    def draw(self):
        GL.glClearColor(0.0, 0.0, 0.0, 0.0)
        GL.glClear(GL.GL_COLOR_BUFFER_BIT)
        GL.glEnableClientState(GL.GL_VERTEX_ARRAY)
        self.vertexbo.bind()
        GL.glVertexPointer(2, GL.GL_FLOAT, 0, self.vertexbo)
        GL.glColor(0 ,1, 0, 1)
        GL.glDrawArrays

if __name__ == '__main__':
    GLUT.glutInit()
    GLUT.glutInitDisplayMode( GLUT.GLUT_DOUBLE | GLUT.GLUT_RGB )
    GLUT.glutInitWindowSize( 250, 250 )
    GLUT.glutInitWindowPosition( 100, 100 )
    GLUT.glutCreateWindow( None )

    demo = DemoVBO()
    GLUT.glutDisplayFunc( demo.draw )
    GLUT.glutIdleFunc( demo.draw )

    GLUT.glutMainLoop()