from OpenGL import GL, GLU, GLUT
import random
import math

class ViewParam:
    # viewing parameters
    SCREEN_DISTANCE = 570
    IOD = 60.0

class DisplayParam:
    # Display Params
    SCREEN_WIDTH = 524
    SCREEN_HEIGHT = 295
    STEREO_MODE = 6
    MULTI_SAMPLE = 8
    SLANT = 0

class Frustum:
    CLIP_NEAR = 0.1
    CLIP_FAR = 10000
    TOP_FRUSTUM = CLIP_NEAR*(DisplayParam.SCREEN_HEIGHT/2)/ViewParam.SCREEN_DISTANCE
    BOTTOM_FRUSTUM = -TOP_FRUSTUM
    RIGHT_FRUSTUM_CYCLOPEAN = CLIP_NEAR*(DisplayParam.SCREEN_WIDTH/2)/ViewParam.SCREEN_DISTANCE
    LEFT_FRUSTUM_CYCLOPEAN = -RIGHT_FRUSTUM_CYCLOPEAN

    DELTA_FRUSTUM = CLIP_NEAR*(ViewParam.IOD/2)/ViewParam.SCREEN_DISTANCE



def fixation_point():
    fixation_point_list = GL.glGenLists(1)
    GL.glNewList(fixation_point_list, GL.GL_COMPILE)
    GL.glPushMatrix()
    GL.glTranslate(0, 0, -ViewParam.SCREEN_DISTANCE)
    GL.glScalef(10, 10, 10)
    GL.glBegin(GL.GL_QUADS)
    GL.glVertex3f(-0.5, -0.5, 0)
    GL.glVertex3f(-0.5, 0.5, 0)
    GL.glVertex3f(0.5, 0.5, 0)
    GL.glVertex3f(0.5, -0.5, 0)
    GL.glEnd()
    GL.glPopMatrix()
    GL.glEndList()
    return fixation_point_list


def planar_surface_list():
    ndots = 100
    planarSurfaceList = GL.glGenLists(1)
    GL.glNewList(planarSurfaceList, GL.GL_COMPILE)
    for i in range(0, ndots):
        GL.glPushMatrix()
        x = 100*(random.uniform(0, 1)-0.5)
        y = 100*(random.uniform(0, 1)-0.5)
        z = -ViewParam.SCREEN_DISTANCE-math.tan(DisplayParam.SLANT) * y
        #print x, y, z
        GL.glTranslatef(x, y, z)
        GL.glScalef(4,4,4)
        GL.glBegin(GL.GL_QUADS)
        GL.glVertex3f(-0.5, -0.5, 0)
        GL.glVertex3f(-0.5, 0.5, 0)
        GL.glVertex3f(0.5, 0.5, 0)
        GL.glVertex3f(0.5, -0.5, 0)
        GL.glEnd()
        GL.glPopMatrix()

    GL.glEndList()
    return planarSurfaceList


def  reshape (width, height ):
    if height == 0:
        height = 1
    GL.glViewport(0,0,width,height)
    GL.glMatrixMode(GL.GL_PROJECTION)
    GL.glLoadIdentity()
    GLU.gluPerspective(45.0, width/height, 0.1, 100.0)

    GL.glMatrixMode(GL.GL_MODELVIEW)
    GL.glLoadIdentity()


def display():

    theEye = -1
    LeftFrustum = Frustum.LEFT_FRUSTUM_CYCLOPEAN - theEye * Frustum.DELTA_FRUSTUM
    RightFrustum = Frustum.RIGHT_FRUSTUM_CYCLOPEAN - theEye * Frustum.DELTA_FRUSTUM

    GL.glMatrixMode(GL.GL_PROJECTION)
    GL.glLoadIdentity()
    GL.glFrustum(LeftFrustum, RightFrustum, Frustum.BOTTOM_FRUSTUM,
              Frustum.TOP_FRUSTUM, Frustum.CLIP_NEAR, Frustum.CLIP_FAR)
    GL.glMatrixMode(GL.GL_MODELVIEW)
    GL.glLoadIdentity()
    GLU.gluLookAt(theEye *ViewParam.IOD/2, 0, 0,
                  theEye * ViewParam.IOD/2, 0, -ViewParam.SCREEN_DISTANCE,
                  0, 1, 0)
    GL.glClear(GL.GL_COLOR_BUFFER_BIT)
    planarSurfaceList = planar_surface_list()
    print planarSurfaceList
    GL.glCallList(planarSurfaceList)
    fixation_point_list =  fixation_point()
    print fixation_point_list
    GL.glCallList(fixation_point_list)

    GL.glFlush()

def init():
    GL.glShadeModel(GL.GL_SMOOTH)
    GL.glClearColor(1.0, 1.0, 1.0, 0.0)
    GL.glColor3f(0.0, 0.0, 0.0)
    GL.glClearDepth(1.0)
    GL.glEnable(GL.GL_DEPTH_TEST)
    GL.glDepthFunc(GL.GL_LEQUAL)
    GL.glHint(GL.GL_PERSPECTIVE_CORRECTION_HINT, GL.GL_NICEST)
    GL.glMatrixMode(GL.GL_PROJECTION)
    GL.glLoadIdentity()
    GLU.gluOrtho2D(0.0,
                   float(DisplayParam.SCREEN_WIDTH), 0.0,
                   float(DisplayParam.SCREEN_HEIGHT))


def redraw():
    pass


if __name__ == '__main__':
    GLUT.glutInit()
    GLUT.glutInitWindowSize(DisplayParam.SCREEN_WIDTH, DisplayParam.SCREEN_HEIGHT)
    GLUT.glutCreateWindow('STIMULI')
    GLUT.glutFullScreen()
    GLUT.glutInitDisplayMode(GLUT.GLUT_RGB)
    GLUT.glutDisplayFunc(display)
    GLUT.glutReshapeFunc(reshape)
    init()
    GLUT.glutMainLoop()

