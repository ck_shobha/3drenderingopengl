from OpenGL import GL, GLUT, GLU
import random
import time

WIDTH = 640
HEIGHT = 480

def display_func():
    GL.glClear(GL.GL_COLOR_BUFFER_BIT)

    for i in range(0, 3):
        gray = 0.3#random.randint(0, 3) / 5.0
        #print gray, idx
        """
        x1 = random.randint(0, WIDTH)
        y1 = random.randint(0, HEIGHT)
        x2 = random.randint(0, WIDTH)
        y2 = random.randint(0, HEIGHT)
        """
        GL.glColor3f(gray, gray, gray)
        """
        GL.glRecti(x1, y1,
                   x2, y2)
        print gray, x1, y1, x2, y2
        """
        GL.glRecti(456, 80, 11, 100)

    GL.glFlush()


def init():
    GL.glClearColor(1.0, 1.0, 1.0, 0.0)
    GL.glColor3f(0.0, 0.0, 0.0)
    GL.glMatrixMode(GL.GL_PROJECTION)
    GL.glLoadIdentity()
    print 'width', float(WIDTH), 'height=', float(HEIGHT)
    GLU.gluOrtho2D(0.0, float(WIDTH), 0.0, float(HEIGHT))


if __name__ == '__main__':
    GLUT.glutInit()
    GLUT.glutInitWindowSize(WIDTH, HEIGHT)
    GLUT.glutCreateWindow('RANDOM SQUARE')
    GLUT.glutInitDisplayMode(GLUT.GLUT_SINGLE | GLUT.GLUT_RGB)
    GLUT.glutDisplayFunc(display_func)
    init()
    GLUT.glutMainLoop(