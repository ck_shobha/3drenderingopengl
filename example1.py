import os
import sys
import numpy
from numpy import array
from ctypes import *
from OpenGL.GL import *
from OpenGL import GLX
from OpenGL.GL import shaders
from OpenGL.arrays import vbo
from OpenGL.raw.GL.ARB.point_sprite import GL_POINT_SPRITE_ARB, GL_COORD_REPLACE_ARB
from OpenGL._bytes import bytes, _NULL_8_BYTE

import Xlib
from Xlib.display import Display
try:
    from OpenGL.GLX import struct__XDisplay
except ImportError as err:
    from OpenGL.raw._GLX import struct__XDisplay
from gi.repository import Gtk, Gdk, GdkX11, GLib, GObject

from PIL import Image

def glDebug():
    error = glGetError()
    if error:
        print ('opengl error ' + str(error))
        return True
    return False


class gtkgl:
    """ wrapper to enable opengl in our gtk application
    useful link http://www.opengl.org/wiki/Programming_OpenGL_in_Linux:_GLX_and_Xlib"""
    #these method do not seem to exist in python x11 library lets exploit the c methods
    xlib = cdll.LoadLibrary('libX11.so')
    xlib.XOpenDisplay.argtypes = [c_char_p]
    xlib.XOpenDisplay.restype = POINTER(struct__XDisplay)
    xdisplay = xlib.XOpenDisplay(None)
    display = Xlib.display.Display()
    attrs = []

    xwindow_id = None
    width, height = 500,300

    def __init__(self):
        """ lets setup are opengl settings and create the context for our window """
        self.add_attribute(GLX.GLX_RGBA, True)
        self.add_attribute(GLX.GLX_RED_SIZE, 8)
        self.add_attribute(GLX.GLX_GREEN_SIZE, 8)
        self.add_attribute(GLX.GLX_BLUE_SIZE, 8)
        self.add_attribute(GLX.GLX_DOUBLEBUFFER, 1)
        self.add_attribute(GLX.GLX_DEPTH_SIZE, 24)

        xvinfo = GLX.glXChooseVisual(self.xdisplay, self.display.get_default_screen(), self.get_attributes())
        print("run glxinfo to match this visual id %s " % hex(xvinfo.contents.visualid))
        self.context = GLX.glXCreateContext(self.xdisplay, xvinfo, None, True)

    def add_attribute(self, setting, value):
        """just to nicely add opengl parameters"""
        self.attrs.append(setting)
        self.attrs.append(value)

    def get_attributes(self):
        """ return our parameters in the expected structure"""
        attrs = self.attrs + [0, 0]
        return (c_int * len(attrs))(*attrs)

    def configure(self, wid):
        """  """
        self.xwindow_id = GdkX11.X11Window.get_xid(wid)
        if(not GLX.glXMakeCurrent(self.xdisplay, self.xwindow_id, self.context)):
            print('failed configuring context')
        glViewport(0, 0, self.width, self.height)

        glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA)
        glEnable(GL_DEPTH_TEST)
        glEnable(GL_BLEND)

        #settings related to enbling pixel sprites.
        glEnable(GL_POINT_SPRITE)
        glEnable(GL_VERTEX_PROGRAM_POINT_SIZE)
        glEnable(GL_PROGRAM_POINT_SIZE)
        glEnable(GL_POINT_SPRITE_ARB)

        #glPointSize(16)

        glShadeModel(GL_SMOOTH)
        glDepthFunc(GL_LEQUAL)

        glPolygonMode(GL_FRONT_AND_BACK, GL_FILL)

    def draw_start(self):
        """make cairo context current for drawing"""
        if(not GLX.glXMakeCurrent(self.xdisplay, self.xwindow_id, self.context)):
            print ("failed to get the context for drawing")

    def draw_finish(self):
        """swap buffer when we have finished drawing"""
        GLX.glXSwapBuffers(self.xdisplay, self.xwindow_id)


def compileShader( source, shaderType ):

    """Compile shader source of given type
        source -- GLSL source-code for the shader
    shaderType -- GLenum GL_VERTEX_SHADER, GL_FRAGMENT_SHADER, etc,
        returns GLuint compiled shader reference
    raises RuntimeError when a compilation failure occurs
    """
    if isinstance(source, str):

        source = [source]
    elif isinstance(source, bytes):

        source = [source.decode('utf-8')]

    shader = glCreateShader(shaderType)
    glShaderSource(shader, source)
    glCompileShader(shader)
    result = glGetShaderiv(shader, GL_COMPILE_STATUS)

    if not(result):
        # TODO: this will be wrong if the user has
        # disabled traditional unpacking array support.
        raise RuntimeError(
            """Shader compile failure (%s): %s"""%(
                result,
                glGetShaderInfoLog( shader ),
            ),
            source,
            shaderType,
        )
    return shader


class shader:
    vertex = """#version 120
        //attributes in values
        attribute vec3 vertex_pos;

        uniform mat4 modelview_mat;
        uniform mat4 projection_mat;

        void main(){
            vec4 pos = modelview_mat * vec4(vertex_pos, 1.0);
            gl_Position = projection_mat * pos;
            gl_PointSize = 16.0;
        }"""

    fragment = """#version 120
        uniform sampler2D quad_texture;
        void main(){
            gl_FragColor = texture2D(quad_texture, gl_PointCoord);
            //gl_FragColor = vec4(gl_TexCoord[0].st, 1, 0.75);//for debug
            //gl_FragColor = vec4(gl_PointCoord, 1, 0.75);//for debug
        }"""

    program = None

    def load_image(self, filename):
        """load our image using pil and setup and make it available to opengl"""
        path = os.path.abspath(filename)
        im = Image.open(path)
        try:

            ix, iy, image = im.size[0], im.size[1], im.tostring("raw", "RGBA", 0, -1)
        except SystemError as error:
            ix, iy, image = im.size[0], im.size[1], im.tostring("raw", "RGBX", 0, -1)
        except:
            return None

        texture_id = glGenTextures(1)
        glDebug()
        glBindTexture(GL_TEXTURE_2D, texture_id)
        glDebug()
        glPixelStorei(GL_UNPACK_ALIGNMENT, 1)
        glDebug()
        glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, ix, iy, 0, GL_RGBA, GL_UNSIGNED_BYTE, image)
        glDebug()

        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR)
        glDebug()
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S,  GL_CLAMP_TO_BORDER)
        glDebug()
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T,  GL_CLAMP_TO_BORDER)
        glDebug()

        if glGetError():
            print(glGetError())

        return texture_id

    def compile(self):
        """setup our shaders and compile for rendering our primitives"""
        self.program = shaders.compileProgram(
            compileShader(self.vertex, GL_VERTEX_SHADER),
            compileShader(self.fragment, GL_FRAGMENT_SHADER),)

        self.point_vertex = glGetAttribLocation(self.program, b'vertex_pos')
        self.texture_uniform = glGetUniformLocation(self.program, b'quad_texture')
        self.point_matrix_model_view = glGetUniformLocation(self.program, b"modelview_mat")
        self.point_matrix_projection = glGetUniformLocation(self.program, b"projection_mat")
        self.fixed_quad_tex_coords = [[[0.0, 1.0]], [[1.0, 1.0]], [[0.0, 0.0]], [[1.0, 0.0]]]
        self.fixed_quad_indices = [0, 1, 2, 1, 2, 3]
        self.fixed_quad_indices_vbo = vbo.VBO(
            array([self.fixed_quad_indices], dtype='uint32'), target=GL_ELEMENT_ARRAY_BUFFER)

class point:
    __slots__ = ['x', 'y', 'z', 'xyz', 'vertex']

    def __init__(self, p, c=(1, 0, 0)):
        """ Position in 3d space as a tuple or list, and colour in tuple or list format"""
        self.x, self.y, self.z = p
        self.vertex = array([self.x, self.y, self.z, c[0], c[1], c[2]], 'f')