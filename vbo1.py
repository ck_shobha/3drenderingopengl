from OpenGL import GL, GLUT, GLU
import numpy as np
from OpenGL.arrays import vbo


vertex_data = np.array([[0.0, 0.5, 0.0],
                           [0.5, -0.5, 0.0],
                           [-0.5, -0.5, 0.0]], dtype=np.float64)

color_data = np.array([[1, 0, 0],
                       ],
                      dtype=np.float64)
def init():
    GL.glClearColor(0.0, 0.0, 0.0, 0.5)
    GL.glClearDepth(1.0)
    GL.glEnable (GL.GL_DEPTH_TEST)
    GL.glDepthFunc(GL.GL_LESS)


class DemoVBO(object):
    def __init__(self, nvert=100, jiggle=0.01):
        points = np.array([[0.0, 0.5, 0.0],
                           [0.5, -0.5, 0.0],
                           [-0.5, -0.5, 0.0]])
        self.vertexbo = vbo.VBO(points)

    def draw(self):
        pass

if __name__ == '__main__':
    GLUT.glutInit()
    GLUT.glutInitDisplayMode( GLUT.GLUT_DOUBLE | GLUT.GLUT_RGB )
    GLUT.glutInitWindowSize( 250, 250 )
    GLUT.glutInitWindowPosition( 100, 100 )
    GLUT.glutCreateWindow( None )

    demo = DemoVBO()
    GLUT.glutDisplayFunc( demo.draw )
    GLUT.glutIdleFunc( demo.draw )
    init()

    GLUT.glutMainLoop()