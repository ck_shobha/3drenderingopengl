from OpenGL import GL, GLU, GLUT

WIDTH=640
HEIGHT=640
MAZEROWS=150
MAZECOLS=150


def init_func():
    GL.glClearColor(1.0, 1.0, 1.0, 0.0)
    GL.glColor3f(0.0, 0.0, 0.0)
    GL.glMatrixMode(GL.GL_PROJECTION)
    GL.glLoadIdentity()
    GLU.gluOrtho2D(-1.0, WIDTH, -1.0, HEIGHT)

def display_func():
    GL.glClear(GL.GL_COLOR_BUFFER_BIT)
    GL.glFlush()


if __name__ == '__main__':
    GLUT.glutInit()
    GLUT.glutInitWindowSize(WIDTH, HEIGHT)
    GLUT.glutCreateWindow('MAZE')
    GLUT.glutInitDisplayMode(GLUT.GLUT_RGB | GLUT.GLUT_SINGLE)
    GLUT.glutDisplayFunc(display_func)
    init_func()
    GLUT.glutMainLoop()

