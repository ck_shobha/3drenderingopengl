import numpy as np
from OpenGL import GL, GLU, GLUT
from OpenGL.arrays import ArrayDatatype
import sys



vertex = """
#version 330
in vec3 vin_position;
in vec3 vin_color;
out vec3 vout_color;

void main(void)
{
    vout_color = vin_color;
    gl_Position = vec4(vin_position, 1.0);
}
"""




vertex_data = np.array([0.75, 0.75, 0.0,
                        0.75, -0.75, 0.0,
                        -0.75, -0.75, 0.0],
                       dtype=np.float64)

color_data = np.array([1, 0, 0,
                       0 ,1, 0,
                       0, 0, 1],
                      dtype=np.float64)

ESCAPE = '\033'

class Triangle(object):
    def __init__(self, window):
        self.glut_window = window


    def init(self):
        GL.glClearColor(0.95, 1.0, 0.95, 0)



    def display(self):
        pass

    def resize(self, width, height):
        if not height:
            height = 1

        # Reset the view port and
        GL.glViewport(0, 0, width, height)
        GL.glMatrixMode(GL.GL_PROJECTION)
        GL.glLoadIdentity()
        GLU.gluPerspective(45.0, float(width)/float(height), 0.1, 100.0)
        GL.glMatrixMode(GL.GL_MODELVIEW)


    def keyboard_activity(self, *args):
        if args[0] == ESCAPE:
            GLUT.glutDestroyWindow(self.glut_window)
            sys.exit()


if __name__ =='__main__':
    # GLUT init
    GLUT.glutInit()
    GLUT.glutInitDisplayMode(GLUT.GLUT_DOUBLE | GLUT.GLUT_RGBA)
    GLUT.glutInitWindowSize(640, 480)
    win = GLUT.glutCreateWindow('Triangle using VBO')
    GLUT.glutFullScreen()

    triangle_obj = Triangle(win)

    GLUT.glutDisplayFunc(triangle_obj.display)

    GLUT.glutIdleFunc(triangle_obj.display)
    GLUT.glutReshapeFunc(triangle_obj.resize)
    GLUT.glutKeyboardFunc(triangle_obj.keyboard_activity)
    triangle_obj.init()
    GLUT.glutMainLoop()


