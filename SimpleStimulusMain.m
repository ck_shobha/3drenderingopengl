function SimpleStimulusMain
global GL;                           

%Viewing Parameters
screenDistance = 570.0;    %mm
IOD = 60.0;                %mm (Interoccular Distance)

%Display Parameters
screenWidth = 524;         %mm
screenHeight = 295;        %mm
stereoMode = 6;            
multiSample = 8;

Slant = 60;

PsychDefaultSetup(2);                 %Calls AssertOpenGL & performs other boilerplate setup operations
InitializeMatlabOpenGL(1);            %Setup PsychToolbox for 3D rendering support; (0,0) disables error checking to speed up processes; (1) initializes MOGL_Wrapper
PsychImaging('PrepareConfiguration'); %Prepare pipeline for configuration.
screenID = max(Screen('Screens'));    %Screen for display.

[window,windowRect] = PsychImaging('OpenWindow',screenID,[0 0 0],[],[],[],stereoMode,multiSample);
[windowCenter(1),windowCenter(2)] = RectCenter(windowRect); %Window center
windowWidth = windowRect(3)-windowRect(1);
windowHeight = windowRect(4)-windowRect(2);
mm2pixX = windowWidth/screenWidth;    %Converts mm to pixels (horizontal)
mm2pixY = windowHeight/screenHeight;  %Converts mm to pixels (vertical)

%psychotoolbox
Screen('BeginOpenGL',window); %Setup the OpenGL rendering context
    glViewport(0,0,windowWidth,windowHeight); %Define viewport
    glDisable(GL.LIGHTING); %Disable lighting; interacts with alpha blending (and often supersedes with unwanted results)
    glEnable(GL.DEPTH_TEST); glDepthFunc(GL.LESS); %Occlusion handling
    glEnable(GL.POINT_SMOOTH); %Enable Point Antialiasing to Make Points Look Like Circles
    glEnable(GL.BLEND); glBlendFunc(GL.SRC_ALPHA,GL.ONE_MINUS_SRC_ALPHA); %Alpha blending for antialising
    glClearColor(0,0,0,0); %Tell glClear to make a gray-scale background that is fully transparent
Screen('EndOpenGL',window);


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%DEFINE FRUSTUM
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%The frustum is determined by projecting the screen boundaries onto the near clipping plane
clipNear = 0.1;     %Near clipping plane (mm)
clipFar = 10000;    %Far clipping plane (mm)

TopFrustum = clipNear*(screenHeight/2)/screenDistance;
BottomFrustum = -TopFrustum;

RightFrustumCyclopean = clipNear*(screenWidth/2)/screenDistance; 
LeftFrustumCyclopean = -RightFrustumCyclopean;

DeltaFrustum = clipNear*(IOD/2)/screenDistance; %The left & right frustum boundaries change depending on the eye.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%DEFINE STRUCTURE HOLDING ALL VIEWING PARAMETERS
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%Viewing Parameters
VP.screenDistance = screenDistance;
VP.IOD = IOD;
VP.stereoMode = stereoMode;

%%Window Parameters
VP.window = window;
VP.windowCenter = windowCenter;
VP.windowWidth = windowWidth;
VP.windowHeight = windowHeight;
VP.screenWidth = screenWidth;
VP.screenHeight = screenHeight;
VP.mm2pixX = mm2pixX;
VP.mm2pixY = mm2pixY;

%%Frustum Parameters
VP.clipNear = clipNear;
VP.clipFar = clipFar;
VP.TopFrustum = TopFrustum;
VP.BottomFrustum = BottomFrustum;
VP.RightFrustumCyclopean = RightFrustumCyclopean;
VP.LeftFrustumCyclopean = LeftFrustumCyclopean;
VP.DeltaFrustum = DeltaFrustum;


fixationPointList = CreateSimpleFixation(VP);
planarSurfaceList = CreateSimplePlane(Slant,VP)

for theEye = [-1 1]
    if -1 == theEye %Left eye
        Screen('SelectStereoDrawBuffer',VP.window,0); %Select buffer for left eye        
    else %Right eye
        Screen('SelectStereoDrawBuffer',VP.window,1); %Select buffer for right eye
    end
    
    LeftFrustum = VP.LeftFrustumCyclopean - theEye*VP.DeltaFrustum; %Define left and right frustum bounds for the selected eye
    RightFrustum = VP.RightFrustumCyclopean - theEye*VP.DeltaFrustum;
    
    Screen('BeginOpenGL',VP.window); %Enable 3D mode 
        glMatrixMode(GL.PROJECTION);
        glLoadIdentity; %Load identity matrix 
        glFrustum(LeftFrustum,RightFrustum,VP.BottomFrustum,VP.TopFrustum,VP.clipNear,VP.clipFar); %Define Frustum
    
        %Switch to Model View and Start Working with the Viewed Objects
        glMatrixMode(GL.MODELVIEW);
        glLoadIdentity; %Load identity matrix
        gluLookAt(theEye*VP.IOD/2,0,0,theEye*VP.IOD/2,0,-VP.screenDistance,0,1,0); %Point the camera       
        glClear; %Clear color and depth buffers
                                      
        glCallList(planarSurfaceList); %Draw planar surface                    
                             
        glCallList(fixationPointList); %Draw the fixation point        
             
    Screen('EndOpenGL',VP.window);
end %%End of eye loop
Screen('DrawingFinished',VP.window); %End all graphics operations until flip
Screen('Flip',VP.window); %Present Stimulus

pause;

Screen('CloseAll'); %close the display window


function fixationPointList = CreateSimpleFixation(VP)
global GL;
Screen('BeginOpenGL',VP.window);
    fixationPointList = glGenLists(1);
    glNewList(fixationPointList,GL.COMPILE);
    glPushMatrix;
    glTranslatef(0,0,-VP.screenDistance);
    glScalef(10,10,10);
        glBegin(GL.QUADS);
            glVertex3f(-0.5,-0.5,0); glVertex3f(-0.5,0.5,0); glVertex3f(0.5,0.5,0); glVertex3f(0.5,-0.5,0); %Vertices of a Unit Square at the Origin              
        glEnd;
    glPopMatrix;
    glEndList;
Screen('EndOpenGL',VP.window);

function planarSurfaceList = CreateSimplePlane(Slant,VP)
global GL;
nDots = 100;
PlaneCoordinates(1,:) = 100*(rand(1,nDots)-0.5);
PlaneCoordinates(2,:) = 100*(rand(1,nDots)-0.5);
PlaneCoordinates(3,:) = -VP.screenDistance-tand(Slant)*PlaneCoordinates(2,:);

Screen('BeginOpenGL',VP.window);
    planarSurfaceList = glGenLists(1);
    glNewList(planarSurfaceList,GL.COMPILE);
    for ithDot = 1:nDots
        glPushMatrix;    
        glTranslatef(PlaneCoordinates(1,ithDot),PlaneCoordinates(2,ithDot),PlaneCoordinates(3,ithDot));        
        glScalef(4,4,4);
        glBegin(GL.QUADS);
            glVertex3f(-0.5,-0.5,0); glVertex3f(-0.5,0.5,0); glVertex3f(0.5,0.5,0); glVertex3f(0.5,-0.5,0);
        glEnd;    
        glPopMatrix;
    end
    glEndList;
Screen('EndOpenGL',VP.window);
