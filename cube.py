from OpenGL import GL, GLU, GLUT
import sys


def display():
    GL.glClear(GL.GL_COLOR_BUFFER_BIT)
    GL.glColor3f(1.0, 1.0, 1.0)
    GL.glLoadIdentity()
    GLU.gluLookAt(0.0, 0.0, 5.0, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0)
    GL.glScalef(1.0, 2.0, 1.0)
    GLUT.glutWireCube(1.0)
    GL.glFlush()


def init():
    GL.glClearColor(0.0, 0.0, 0.0, 0.0)
    GL.glShadeModel(GL.GL_FLAT)


def reshape(w, h):
    GL.glViewport(0, 0, w, h)
    GL.glMatrixMode(GL.GL_PROJECTION)
    GL.glLoadIdentity()
    GL.glFrustum(-2.0, 4.0, -2.0, 4.0, 1.5, 20.0)
    GL.glMatrixMode(GL.GL_MODELVIEW)


def keyboard(key, x, y):
    if key == chr(27):
        sys.exit(0)


if __name__ == '__main__':
    GLUT.glutInit(sys.argv)
    GLUT.glutInitDisplayMode(GLUT.GLUT_DEPTH | GLUT.GLUT_RGB)
    GLUT.glutInitWindowSize(500, 500)
    GLUT.glutInitWindowPosition(200, 200)
    GLUT.glutCreateWindow('CUBE')
    init()
    GLUT.glutDisplayFunc(display)
    GLUT.glutReshapeFunc(reshape)
    GLUT.glutKeyboardFunc(keyboard)
    GLUT.glutMainLoop()

