import OpenGL
from OpenGL.GL import *
from OpenGL.GLUT import *
from OpenGL.GLU import *

mesh_resolution = 4.0
mesh_height_scale = 1.0

GL_ARRAY_BUFFER_ARB = 0x8892
GL_STATIC_DRAW_ARB = 0x88E4

glGenB