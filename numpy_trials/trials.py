import numpy as np

no_of_points = 20

print "Array manipulation"
def numpyversion():
    x = np.arange(no_of_points)
    y = np.arange(no_of_points)
    z = x+ y
    print 'x= ', x
    print 'y= ', y
    print 'z= ', z

numpyversion()

#linespace
print "linspace"
lp = np.linspace(1, 10)
print lp
lp1 = np.linspace(1, 10, endpoint=False, retstep=True)
print lp1
lp2 = np.linspace(1, 11, 7, endpoint=False)
print lp2
lp2 = np.linspace(1, 11, 7)
print lp2
lp2 = np.linspace(1, 11, 7, endpoint=False, retstep=True)
print lp2
lp2 = np.linspace(1, 11, 7, retstep=True)
print lp2

# arrange
print "arrange"
a = np.arange(10)
print a
b = np.arange(2, 10, 2)
print b
c = np.arange(4.5, 6.5, 1, int)
print c

# array
a = [1, 2, 3, 4.5]
array_list = np.array(a)
print array_list



# 0 dim array
print "Array dimensions"
x =np.array(42)
print (type(x))
print 'x= ', x
print 'np.ndim(x) = ', np.ndim(x)


# 1 dim array
x = np.array([1,2, 3, 4])
y = np.array([1.1, 2.2, 3.3])
print x.dtype, y.dtype
print np.ndim(x), np.ndim(y)

print 'Multi dimentions'
A = np.array([[1, 2, 3],
             [2, 3, 4],
             [5, 6, 7]])
print A.shape
print "access = ", A[0, 2]

B = np.array([[[111, 112], [121, 122]],
              [[211, 212], [221, 222]],
              [[311, 312], [321, 322]]])

print "B= ", B.shape
print "access= ", B[0, 1, 1]
print "access= ", B[2, 1, 0]

B.shape = (2, 2, 3)
print "B reshaped= ", B



print " SLICING"
A = np.array([1, 2, 3, 4, 5])
S = A[1:3]
S[0] = 22
S[1] = 23
print "A = ", A


print "EYE"
k = np.eye(5, 8, k=-1, dtype=int)
print "eye = \n", k


# randome array
x = np.random.rand(4, 3, dtype=int)