from OpenGL import GL
from OpenGL import GLUT
from OpenGL import GLU


def display_func():
    GL.glClear(GL.GL_COLOR_BUFFER_BIT)
    GL.glBegin(GL.GL_POINTS)
    GL.glVertex2i(100, 50)
    GL.glVertex2i(100, 130)
    GL.glVertex2i(150, 130)
    GL.glEnd()
    GL.glFlush()

def initFun():
        GL.glClearColor(1.0, 1.0, 1.0, 0.0)
        GL.glColor3f(0.0, 0.0, 0.0)
        GL.glPointSize(10.0)
        GL.glMatrixMode(GL.GL_PROJECTION)
        GL.glLoadIdentity()
        GLU.gluOrtho2D(0.0,640.0,0.0,480.0)

if __name__ == '__main__':
    GLUT.glutInit()
    GLUT.glutInitWindowSize(640, 480)
    GLUT.glutCreateWindow('Test Window')
    GLUT.glutInitDisplayMode(GLUT.GLUT_SINGLE)
    GLUT.glutDisplayFunc(display_func)
    initFun()
    GLUT.glutMainLoop()



