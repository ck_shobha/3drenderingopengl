

#!/usr/bin/env python
import os
import sys
import time
import random
import pprint

import Xlib
from Xlib.display import Display
from gi.repository import Gtk, Gdk, GdkX11, GLib, GObject
from numpy import array

from OpenGL.GL import *
from OpenGL.GLU import gluPerspective, gluLookAt
from OpenGL.arrays import vbo
from OpenGL import GLX

from OpenGL.GL import GL_VERTEX_SHADER, GL_FRAGMENT_SHADER
from OpenGL.GL import shaders, glGetUniformLocation


from example1 import shader, gtkgl


class scene:
    width, height = 600, 400
    camera_distance = 25
    texture_id = None

    def __init__(self):
        """setup everything in the correct order"""
        self.glwrap = gtkgl()
        self.setup_opengl()
        self.generate()
        self.gui()

    def gui(self):
        """load in the gui and connect the events and set our properties"""
        self.start_time = time.time()
        self.frame = 1
        xml = Gtk.Builder()
        xml.add_from_file('gui.glade')

        self.window = xml.get_object('window1')
        self.button = xml.get_object('btngenerate')

        self.canvas_widget = xml.get_object('canvas')
        self.canvas_widget.connect('configure_event', self.on_configure_event)
        self.canvas_widget.connect('draw', self.on_draw)
        self.canvas_widget.set_double_buffered(False)
        self.canvas_widget.set_size_request(self.glwrap.width, self.glwrap.height)

        self.button.connect('pressed', self.generate)

        self.window.show_all()
        self.setup_shaders()
        GObject.idle_add(self.loop_draw)


    def loop_draw(self):
        #send redraw event to drawing area widget
        self.canvas_widget.queue_draw()
        return True

    def on_configure_event(self, widget, event):
        """if we recieve a configure event for example a resize, then grab the context settings and resize our scene """
        self.glwrap.width = widget.get_allocation().width
        self.glwrap.height = widget.get_allocation().height
        self.width, self.height = self.glwrap.width, self.glwrap.height

        #update our states because we have reconfigured the display
        self.glwrap.configure(widget.get_window())
        self.glwrap.draw_start()
        self.update_camera()


        glEnable(GL_TEXTURE_2D)
        glEnable(GL_DEPTH_TEST)
        glDepthMask(GL_TRUE)
        glDepthFunc(GL_LEQUAL)
        glDepthRange(0.0, 1.0)
        glEnable(GL_CULL_FACE)
        glCullFace(GL_BACK)
        glFrontFace(GL_CW)

        self.glwrap.draw_finish()
        return True

    def on_draw(self, widget, context):
        """if we recieve a draw event redraw our opengl scene"""
        self.elapsed_time = time.time() - self.start_time
        self.frame += 1

        if self.elapsed_time > 1:
            print('fps %d' % self.frame)
            self.start_time = time.time()
            self.frame = 1
        self.glwrap.draw_start()
        #self.draw()
        glClearDepth(1.0)
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT)
        glLoadIdentity()
        self.draw_shader_points_texture()
        self.glwrap.draw_finish()

    def generate(self, *args):
        """randomly position a few textured points"""
        self.point_sprites = []
        for i in range(0, 10):
            self.point_sprites.append((random.uniform(-8, 8), random.uniform(-8, 8), random.uniform(-8, 8)))

        print('Generated %s points' % str(len(self.point_sprites)))
        self.vertex_vbo = vbo.VBO(array(self.point_sprites, 'f'))

    def setup_shaders(self):
        self.shader_program = shader()
        self.shader_program.compile()
        self.texture_id = self.shader_program.load_image('testing.png')

    def setup_opengl(self):
        glShadeModel(GL_SMOOTH)
        glClearColor(0.0, 0.0, 0.0, 0.0)
        glClearDepth(1.0)
        glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST)
        glPolygonMode(GL_FRONT_AND_BACK, GL_FILL)

    def update_camera(self):
        """Setup a very basic camera"""
        glViewport(0, 0, self.width, self.height)
        glMatrixMode(GL_PROJECTION)
        glLoadIdentity()
        gluPerspective(45, 1.0 * self.width / self.height, 1.0, 80.0)
        gluLookAt(self.camera_distance, self.camera_distance, self.camera_distance,     # location
                  0.0, 0.0, 0.0,        # lookat
                  0.0, 1.0, 0.0)        # up direction
        glMatrixMode(GL_MODELVIEW)
        glLoadIdentity()

    def draw_shader_points_texture(self):
        glEnableClientState(GL_VERTEX_ARRAY)
        glEnableClientState(GL_COLOR_ARRAY)
        glEnableClientState(GL_TEXTURE_COORD_ARRAY)
        matrix_model_view = glGetFloatv(GL_MODELVIEW_MATRIX)
        matrix_projection = glGetFloatv(GL_PROJECTION_MATRIX)

        glUseProgram(self.shader_program.program)
        self.vertex_vbo.bind()
        glEnableVertexAttribArray(self.shader_program.point_vertex)
        glVertexAttribPointer(self.shader_program.point_vertex, 3, GL_FLOAT, GL_FALSE, 12, self.vertex_vbo)

        #send the model and projection matrices to the shader
        glUniformMatrix4fv(self.shader_program.point_matrix_model_view, 1, GL_FALSE, matrix_model_view)
        glUniformMatrix4fv(self.shader_program.point_matrix_projection, 1, GL_FALSE, matrix_projection)

        #make the texture we loaded in on shader initalisation active, passing the texture id supplied a t this
        glActiveTexture(GL_TEXTURE0)
        glBindTexture(GL_TEXTURE_2D, self.texture_id)
        glUniform1i(self.shader_program.texture_uniform, 0)

        glDrawArrays(GL_POINTS, 0, len(self.vertex_vbo))

        glDisableVertexAttribArray(self.shader_program.point_vertex)
        self.vertex_vbo.unbind()
        glDisableClientState(GL_COLOR_ARRAY)
        glDisableClientState(GL_VERTEX_ARRAY)
        glDisableClientState(GL_TEXTURE_COORD_ARRAY)

    def draw(self):
        glEnable(GL_DEPTH_TEST)
        glClearDepth(1.0)
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT)
        glLoadIdentity()
        self.draw_shader_points_texture()


if __name__ == '__main__':
    glexample = scene()
    Gtk.main()
